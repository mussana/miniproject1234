import 'package:flutter/material.dart';
 
void main() {
  runApp(MyApp());
}
 
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}
 
class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('ระบบบำรุงรักษาเครื่องปรับอากาศ'),
          ),
          body: Center(
              child: Column(children: <Widget>[
            Container(
              margin: EdgeInsets.all(10),
              child: Tooltip(
                message: 'แสกนคิวอาร์โค้ด',
                child: FlatButton(
                  child: Icon(
                    Icons.photo_camera,
                    size: 50,
                  ),
              )),
            ),
             Container(
              margin: EdgeInsets.all(20),
              child: FlatButton(
                child: Text('แสกนคิวอาร์โค้ด'),
                color: Colors.blueAccent,
                textColor: Colors.white,
                onPressed: () {},
              ),
            ),
                   
          ]))),
      
    );
  }
}