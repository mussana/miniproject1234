import 'package:flutter/material.dart';
void main() {
  runApp(MyApp());
}
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}
class _MyAppState extends State<MyApp> {
  TextEditingController nameController = TextEditingController();
  String id = '';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('ระบบบำรุงรักษาเครื่องปรับอากาศ'),
          ),
          body: Center(child: Column(children: <Widget>[
            Container(
                margin: EdgeInsets.all(1),
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'รหัส',
                  ),
                )),
             Container(
                margin: EdgeInsets.all(5),
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'รายละเอียดเครื่อง',
                  ),
                )),
            Container(
                margin: EdgeInsets.all(20),
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'ห้อง',
                  ),
                )),
            Container(
                margin: EdgeInsets.all(5),
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'รายละเอียดการซ่อม',
                  ),
                )),
             Container(
                margin: EdgeInsets.all(20),
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'ผู้ซ่อม',
                  ),
                )),
            Container(
                margin: EdgeInsets.all(5),
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'ร้านที่ซ่อม',
                  ),
                )),
            Container(
                margin: EdgeInsets.all(5),
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'วันที่ซ่อม',
                  ),
                )),
             Container(
              margin: EdgeInsets.all(20),
              child: FlatButton(
                child: Text('บันทึก'),
                onPressed: () {},
              ),
            ),
             RaisedButton(
                child: Text('ยกเลิก'),
                textColor: Colors.red,
                onPressed: () {},
              ),
          ]))),
    );
  }
}